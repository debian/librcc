Source: librcc
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 debhelper-compat (= 13),
 automake,
 doxygen,
 libaspell-dev,
 libdb-dev,
 libenca-dev,
 librcd-dev,
 libtool,
 libxml2-dev,
 pkg-config
Standards-Version: 4.6.1
Section: libs
Rules-Requires-Root: no
Homepage: http://rusxmms.sourceforge.net/
Vcs-Git: https://salsa.debian.org/debian/librcc.git
Vcs-Browser: https://salsa.debian.org/debian/librcc

Package: librcc0
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: RusXMMS Charset Conversion library
 libRCC allows developers to work with multiple encodings of the same language
 by adapting the encoding of ID3 tags, M3U and PLS playlists (including file
 names) to local settings on the fly.
 .
 The library is not limited to ID3 tags and can be used by any program that
 works with small titles or file names in different languages and encodings.
 .
  * Language auto-detection.
  * On-the-fly translation between languages, using online-services.
  * Encoding auto-detection for most European Languages.
  * Support for encoding detection plugins (besides Enca and LibRCD).
  * Recoding/translation of multi-language playlists.
  * Cache to speed-up re-recoding.
  * Potential to configure new languages and encodings.
  * Shared configuration file.
  * Menu localization.

Package: librccui0
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: RusXMMS Charset Conversion library (user interface)
 libRCC allows developers to work with multiple encodings of the same language
 by adapting the encoding of ID3 tags, M3U and PLS playlists (including file
 names) to local settings on the fly.
 .
 The library is not limited to ID3 tags and can be used by any program that
 works with small titles or file names in different languages and encodings.
 .
  * Language auto-detection.
  * On-the-fly translation between languages, using online-services.
  * Encoding auto-detection for most European Languages.
  * Support for encoding detection plugins (besides Enca and LibRCD).
  * Recoding/translation of multi-language playlists.
  * Cache to speed-up re-recoding.
  * Potential to configure new languages and encodings.
  * Shared configuration file.
  * Menu localization.
 .
 This package contains the libRCC UI library.

Package: librcc-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 librcc0 (= ${binary:Version}),
 librccui0 (= ${binary:Version}),
 librcd-dev,
 ${misc:Depends}
Description: RusXMMS Charset Conversion library (development files)
 libRCC allows developers to work with multiple encodings of the same language
 by adapting the encoding of ID3 tags, M3U and PLS playlists (including file
 names) to local settings on the fly.
 .
 The library is not limited to ID3 tags and can be used by any program that
 works with small titles or file names in different languages and encodings.
 .
  * Language auto-detection.
  * On-the-fly translation between languages, using online-services.
  * Encoding auto-detection for most European Languages.
  * Support for encoding detection plugins (besides Enca and LibRCD).
  * Recoding/translation of multi-language playlists.
  * Cache to speed-up re-recoding.
  * Potential to configure new languages and encodings.
  * Shared configuration file.
  * Menu localization.
 .
 This package contains the development files (headers and libraries)
 needed to develop programs using libRCC.

Package: librcc-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Replaces: librcc-dev (<< 0.2.13-1)
Breaks: librcc-dev (<< 0.2.13-1)
Description: RusXMMS Charset Conversion library (documentation)
 libRCC allows developers to work with multiple encodings of the same language
 by adapting the encoding of ID3 tags, M3U and PLS playlists (including file
 names) to local settings on the fly.
 .
 The library is not limited to ID3 tags and can be used by any program that
 works with small titles or file names in different languages and encodings.
 .
  * Language auto-detection.
  * On-the-fly translation between languages, using online-services.
  * Encoding auto-detection for most European Languages.
  * Support for encoding detection plugins (besides Enca and LibRCD).
  * Recoding/translation of multi-language playlists.
  * Cache to speed-up re-recoding.
  * Potential to configure new languages and encodings.
  * Shared configuration file.
  * Menu localization.
 .
 This package contains the libRCC development documentation and examples.
